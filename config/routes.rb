AccountManager::Application.routes.draw do
  resources :cashflows
  resources :clients
  devise_for :users
  root :to => 'application#index'
  get 'show_image/:id' => 'user_profile#show_image'
  get 'user_profile/index'
  
  match '/setting' => "user_profile#edit", :as => :profile ,via: [:get, :patch]
  match 'profile/edit' => "user_profile#update", via: [ :put, :patch ], :as => :update_profile
  match 'profile/update_password'=> "user_profile#update_password", via: [:patch],:as => :update_password
  match 'profile/update_secret_password' => 'user_profile#update_secret_password', via: [:patch], :as => :update_secret_password

  match 'client_cash_flow_detail/:id' => "clients#cashflow_detail", via: [:get], :as => :cashflow_detail
  
  # Search route
  get 'search/new'
  match 'search/generate_report'=> "search#generate_report", via: [:post],:as => :generate_report


  match 'new_cashflow_credit'=>"cashflows#new",via: [:get],:as => :new_cashflow_credit
  match 'new_cashflow_debit'=>"cashflows#new",via: [:get],:as => :new_cashflow_debit


  #  Delete Validation Path
  match '/confirmation_secrate' => 'application#confirmation_secrate', :as => "confirmation", via: [:get]
  match '/confirmation_sec' => 'clients#confirmation_secrate', :as => "confirmation_cash", via: [:get]

  match '/confirmation_delete' => 'clients#confirmation_delete', :as => "confirmation_delete", via: [:get]
  match '/confirmation_cash_delete' => 'cashflows#confirmation_cash_delete', :as => "confirmation_cash_delete", via: [:get]

  
end