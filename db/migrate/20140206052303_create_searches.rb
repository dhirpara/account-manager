class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.date :date_from
      t.date :date_to
      t.integer :client_id
      t.string :cash_flow_type

      t.timestamps
    end
  end
end
