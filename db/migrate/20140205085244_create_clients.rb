class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :client_name
      t.string :email
      t.string :phone_no
      t.string :city
      t.string :state
      t.string :country
      t.text :address

      t.timestamps
    end
  end
end
