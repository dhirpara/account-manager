class AddClientIdToCashflow < ActiveRecord::Migration
  def change
    add_column :cashflows, :client_id, :int
  end
end
