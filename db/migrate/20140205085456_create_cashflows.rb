class CreateCashflows < ActiveRecord::Migration
  def change
    create_table :cashflows do |t|
      t.string :cash_type
      t.date :date
      t.float :amount
      t.text :description

      t.timestamps
    end
  end
end
