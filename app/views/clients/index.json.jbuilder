json.array!(@clients) do |client|
  json.extract! client, :id, :client_name, :email, :phone_no, :city, :state, :country, :address
  json.url client_url(client, format: :json)
end
