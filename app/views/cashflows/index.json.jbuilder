json.array!(@cashflows) do |cashflow|
  json.extract! cashflow, :id, :type, :date, :amount, :description
  json.url cashflow_url(cashflow, format: :json)
end
