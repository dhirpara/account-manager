class ClientsController < ApplicationController
  before_action :set_client, only: [:show, :edit, :update, :destroy, :cashflow_detail]

  # GET /clients
  # GET /clients.json
  def index
    @clients = current_user.clients.all
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
  end
  
  def cashflow_detail
    @credit_amount = @client.cashflows.where(:cash_type => "Credit").sum(:amount)
    @debit_amount = @client.cashflows.where(:cash_type => "Debit").sum(:amount)
    @outstanding_amount = @credit_amount-@debit_amount
  end

  def confirmation_secrate
    @id = params[:id]
    @secrate = params[:secret_password]
    @secret_password = current_user.secret_password

    if @secrate == @secret_password
      @cashflow = Cashflow.find_by_id(@id)
      @cashflow.destroy
      flash[:notice] = " Successfully Deleted"
      redirect_to root_path
    else
      @cashflow = Cashflow.find_by_id(@id)
      @client = @cashflow.client.id
      flash[:error_notice]="Password was wrong."
      redirect_to cashflow_detail_path(@client)
    end
  end

  def confirmation_delete
    @id = params[:id]
    @secrate = params[:secret_password]
    @secret_password = User.first.secret_password

    if @secrate == @secret_password
      @client = Client.find_by_id(@id)
      @client.destroy
      flash[:notice] = "Successfully Deleted"
      redirect_to clients_path
    else
      flash[:error_notice]="Password was wrong."
      redirect_to clients_path
    end
  end

  # GET /clients/new
  def new
    @client = current_user.clients.new
  end

  # GET /clients/1/edit
  def edit
  end

  # POST /clients
  # POST /clients.json
  def create
    @client = current_user.clients.new(client_params)

    respond_to do |format|
      if @client.save
        format.html { redirect_to root_path, notice: 'Client was successfully created.' }
        format.json { render action: 'show', status: :created, location: @client }
      else
        format.html { render action: 'new' }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to root_path, notice: 'Client was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client.destroy
    respond_to do |format|

      format.html { redirect_to root_path }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:user_id, :client_name, :email, :phone_no, :city, :state, :country, :address)
    end
end
