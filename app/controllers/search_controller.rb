class SearchController < ApplicationController
  respond_to :js
  
  def new
  end
  
  def generate_report
    year = params[:date_from]["written_on(1i)"]
    month = params[:date_from]["written_on(2i)"]
    date = params[:date_from]["written_on(3i)"]
    if month.to_i < 10
      month = '0'+month
    end 
    @date_from = year+'-'+month+'-'+date
    
    tyear = params[:date_to]["written_to(1i)"]
    tmonth= params[:date_to]["written_to(2i)"]
    tdate = params[:date_to]["written_to(3i)"]
    if tmonth.to_i < 10
      tmonth = '0'+tmonth
    end
    @date_to = tyear+'-'+tmonth+'-'+tdate
    @clients = current_user.clients.all
    @client = params[:client_id]
    @cash_flow_type = params[:cashflow_type]
    from_date = @date_from
    to_date = @date_to
    if @cash_flow_type == "Credit"
      @result = current_user.cashflows.where( 
        :client_id => @client,
        :cash_type => @cash_flow_type,
        :date => from_date..to_date
      )
    elsif @cash_flow_type == "Debit"
      @result = current_user.cashflows.where( 
        :client_id => @client,
        :cash_type => @cash_flow_type,
        :date => from_date..to_date
      )
    else
      @result = current_user.cashflows.where( 
        :client_id => @client,
        :date => from_date..to_date
      )
    end
  end
end