class UserProfileController < ApplicationController
  before_filter :authenticate_user!, :only => [:edit, :update, :index]

  def index
    @user_profile = current_user
  end

  def edit
    @user_profile = current_user
  end

  def show_image
    @user = User.find(params[:id])
    send_data @user.profile_pic_data, :type => @user.profile_pic_content_type, :disposition => 'inline'
  end


  def update
    @upload_img = params[:user][:profile_pic]
    @user_profile = current_user
    respond_to do |format|
      if @user_profile.update_attributes(user_params)
        if !@upload_img.blank?
          @user_profile.update_attributes({:profile_pic_data => @upload_img.tempfile.read, :profile_pic_file_name => @upload_img.original_filename, :profile_pic_file_size => @upload_img.tempfile.size, :profile_pic_content_type => @upload_img.content_type})
        end
        format.html { redirect_to profile_path}
        flash[:notice] = 'Your information has been saved.'
        format.json { head :no_content }
      else
        format.html { render "edit" }
        format.json { render json: @user_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_password
    @user = User.find(current_user.id)
    respond_to do |format|
      if @user.update_with_password(password_params)
        sign_in @user, :bypass => true
        flash[:notice] = "Your Password is successfully changed"
        format.html { redirect_to profile_path}
      else
        flash[:error_notice] = "Your Password not change"
        render "edit"
      end
    end
  end

  def update_secret_password
    @user = User.find(current_user.id)
    if update_secret_password_params[:secret_password] == update_secret_password_params[:secret_password_confirmation]
      @user.update_attributes(:secret_password => update_secret_password_params[:secret_password])
      flash[:notice] = 'Secret Password saved successfully.'
      redirect_to profile_path
    else
      flash[:error_notice] = "Secret Password not match"
      redirect_to profile_path
    end
  end


  private
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :secret_password, :secret_password_confirmation)
    end

    def password_params
      params.require(:user).permit(:current_password,:password, :password_confirmation) 
    end

    def update_secret_password_params
      params.require(:user).permit(:secret_password, :secret_password_confirmation)
    end
end