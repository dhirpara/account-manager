class CashflowsController < ApplicationController
  before_action :set_cashflow, only: [:show, :edit, :update, :destroy]

  # GET /cashflows
  # GET /cashflows.json
  def index
    @cashflows = current_user.cashflows.all
  end

  # GET /cashflows/1
  # GET /cashflows/1.json
  def show
  end

  def confirmation_cash_delete
    @id = params[:id]
    @secrate = params[:secret_password]
    @secret_password = current_user.secret_password

    if @secrate == @secret_password
      @cashflow = Cashflow.find_by_id(@id)
      @cashflow.destroy
      flash[:notice] = "Successfully Deleted"
      redirect_to cashflows_path
    else
      @cashflow = Cashflow.find_by_id(@id)
      @client = @cashflow.client.id
      flash[:error_notice]="Password was wrong."
      redirect_to cashflows_path
    end
  end

  # GET /cashflows/new
  def new
    @cashflow = current_user.cashflows.new
  end

  # GET /cashflows/1/edit
  def edit
  end

  # POST /cashflows
  # POST /cashflows.json
  def create
    @cashflow = current_user.cashflows.new(cashflow_params)

    respond_to do |format|
      if @cashflow.save
        format.html { redirect_to root_path, notice: 'Cashflow was successfully created.' }
        format.json { render action: 'show', status: :created, location: @cashflow }
      else
        format.html { render action: 'new' }
        format.json { render json: @cashflow.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cashflows/1
  # PATCH/PUT /cashflows/1.json
  def update
    respond_to do |format|
      if @cashflow.update(cashflow_params)
        format.html { redirect_to root_path, notice: 'Cashflow was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @cashflow.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cashflows/1
  # DELETE /cashflows/1.json
  def destroy
    @cashflow.destroy
    respond_to do |format|
      format.html { redirect_to cashflows_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cashflow
      @cashflow = Cashflow.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cashflow_params
      params.require(:cashflow).permit(:user_id, :cash_type, :date, :amount, :description, :client_id)
    end
end
