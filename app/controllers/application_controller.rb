class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  
  before_filter :authenticate_user!


  def index
    session[:client_id] = nil
    session[:cashflow_id] = nil

  	@total_credit = current_user.cashflows.where(:cash_type => "Credit").sum(:amount)
  	@total_debit = current_user.cashflows.where(:cash_type => "Debit").sum(:amount)
  	@outstanding = @total_credit - @total_debit
  end


  def confirmation_secrate
    @id = params[:id]
    @secrate = params[:secret_password]
    @secret_password = current_user.secret_password

    if @secrate == @secret_password
      @client = Client.find_by_id(@id)
      @client.destroy
      flash[:notice] = "Successfully Deleted"
      logger.info "<------------#{request.path}-------------------->"
      redirect_to root_path
    else
      flash[:error_notice]="Password was wrong."
      redirect_to root_path
    end
  end
  private
    def mobile_device?
      request.user_agent =~ /Mobile|webOS/
    end
    helper_method :mobile_device?
end
