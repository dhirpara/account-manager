// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.cookie
//= require jPushMenu
//= require jquery.nanoscroller
//= require jquery.sparkline.min
//= require jquery-ui
//= require jquery.gritter
//= require core
//= require codemirror
//= require xml
//= require css
//= require htmlmixed
//= require matchbrackets
//= require jquery-jvectormap-1.2.2.min
//= require jquery-jvectormap-world-mill-en
//= require dashboard
//= require voice-commands
//= require bootstrap.min
//= require jquery.flot
//= require jquery.flot.pie
//= require jquery.flot.resize
//= require jquery.flot.labels
//= require_tree .

$("#report_tab").hide();