class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :clients
  has_many :cashflows
  # validates_confirmation_of :secret_password
  # validates_presence_of :secret_password_confirmation
end
