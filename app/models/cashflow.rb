class Cashflow < ActiveRecord::Base
	belongs_to :client
	belongs_to :user
	validates :cash_type, :client_id, presence: true

end
