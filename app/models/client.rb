class Client < ActiveRecord::Base
	has_many :cashflows, dependent: :destroy
	belongs_to :user
end
